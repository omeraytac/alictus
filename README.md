# Alictus Task

This is a demonstration of automation task. Therefore, running this task.py will create a simple dataset and will work on it. 

Steps of demonstration:

1 - Connect to Google Drive API and Google Sheets API.

2 - Create new dataset with 4 rows if it does not exist.

3 - Create a folder "alictus" in Google Drive.

4 - Upload dataset to Google Drive.

5 - Create spreadsheet files for every campaign.

6 - Add total cost column and its value to every spreadsheet file by using sheet formula.


## How to run?

1 - Follow the steps in the link https://developers.google.com/workspace/guides/create-project and create a project. 

2 - On your project dashboard, navigate to API & Services -> Credentials. 

3 - Create an OAuth Client ID Credential for a web application. While creating credential, add http://localhost:8080/ to Authorized redirect URIs. 

4 - Download JSON file of OAuth Credential into the same directory where task.py file is located. Rename it as "credentials.json".

4 - Navigate to API & Services -> OAuth consent screen. Add your email as a test user. This will allow you to work with the project with your account. 

5 - In API & Services -> OAuth consent screen, in scope step, add 'https://www.googleapis.com/auth/drive' as scope to have access to everything in drive.

5 - Open your command prompt. Run "python < path of task.py >" 