from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
import pandas as pd
import json

SCOPES = ['https://www.googleapis.com/auth/drive']

def create_new_dataset():
	print("create_new_dataset")
	if not os.path.exists('dataset.csv'):
		df = pd.DataFrame(columns=["Campaign name", "Total Impression", "Total Clicks", "CTR (%)", "CPC", "Total App Install"])
		df.to_csv("dataset.csv" ,index=False)

def fill_dataset():
	print("fill_dataset")
	df = pd.read_csv('dataset.csv')
	if df.shape[0] == 0:
		row1 = {"Campaign name":"A", "Total Impression":"106622", "Total Clicks":"879", "CTR (%)":"0,82", "CPC":"0,7106916", "Total App Install":"626"}
		row2 = {"Campaign name":"B", "Total Impression":"95687", "Total Clicks":"805", "CTR (%)":"0,84", "CPC":"0,5509020", "Total App Install":"506"}
		row3 = {"Campaign name":"C", "Total Impression":"12157", "Total Clicks":"102", "CTR (%)":"0,84", "CPC":"0,5960410", "Total App Install":"57"}
		row4 = {"Campaign name":"D", "Total Impression":"6006", "Total Clicks":"64", "CTR (%)":"1,07", "CPC":"0,7419186", "Total App Install":"47"}
		df = df.append(row1, ignore_index = True)
		df = df.append(row2, ignore_index = True)
		df = df.append(row3, ignore_index = True)
		df = df.append(row4, ignore_index = True)
		print(df)
		df.to_csv("dataset.csv" ,index=False)

def upload(parent_dir, metadata, uploadpath, mimetype, items, service):
	print("upload")
	dataset_exists = False
	folder_id = "."
	print(metadata.split("/")[-1][:-4])
	for item in items:
		if item["name"] == metadata.split("/")[-1][:-4]:
			dataset_exists = True
			folder_id  = item["id"]
			print("dataset exists")
			break
	if not dataset_exists:
		file_metadata = {'name': metadata, "parents":[{"id":folder_id}]}
		media = MediaFileUpload(uploadpath, mimetype=mimetype)
		file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
		print('File ID: %s' % file.get('id'))

def create_folder(name, items, service):
	print("create_folder")
	folder_exists = False
	for item in items:
		if item["name"] == name:
			folder_exists = True
			print("create_folder - folder exists")
			break
	if not folder_exists:
		file_metadata = {
		    'name': name,
		    'mimeType': 'application/vnd.google-apps.folder'
		}
		file = service.files().create(body=file_metadata,
		                                    fields='id').execute()
		print('Folder ID: %s' % file.get('id'))

def create_spreadsheet(title, items, datasheets_service, service):
	print("create_spreadsheet")
	sheet_exists = False
	for item in items:
		if item["name"] == title + ".xlsx":
			sheet_exists = True
			print("sheet exists")
			break
	if not sheet_exists:
		spreadsheet = {
		    'properties': {
		    	'title': title
		    }
		}
		spreadsheet = datasheets_service.spreadsheets().create(body=spreadsheet,
		                                    fields='spreadsheetId').execute()
		print('Spreadsheet ID: {0}'.format(spreadsheet.get('spreadsheetId')))
		return spreadsheet.get("spreadsheetId")

def update_spreadsheet(sheet_id, df, index, items, datasheets_service, service):
	print("update_spreadsheet - ", sheet_id)
	values = [
	    [
	        "Campaign name", "Total Impression", "Total Clicks", "CTR (%)", "CPC", "Total App Install", "total budget"
	    ],
	    [
	        str(df["Campaign name"].values[index]), str(df["Total Impression"].values[index]), str(df["Total Clicks"].values[index]), 
	        str(df["CTR (%)"].values[index]), str(df["CPC"].values[index]), str(df["Total App Install"].values[index]), "=C2*E2"
	    ]
	]
	body = {
	    'values': values
	}
	result = datasheets_service.spreadsheets().values().update(
	    spreadsheetId=str(sheet_id), range="A1",
	    valueInputOption="USER_ENTERED", body=body).execute()
	print('{0} cells updated.'.format(result.get('updatedCells')))

def list_files(service):
	print("list_files")
	results = service.files().list(
        fields="nextPageToken, files(id, name)").execute()
	items = results.get('files', [])
	if not items:
		print('No files found.')
	else:
		print('Files:')
		for item in items:
			print(u'{0} ({1})'.format(item['name'], item['id']))
	return items

def main():
	# connect api
    creds = None
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

    service = build('drive', 'v3', credentials=creds)

    datasheets_service = build('sheets', 'v4', credentials=creds)

    # prepare an example dataset for test purpose
    create_new_dataset()
    fill_dataset()

    # Call the Drive v3 API

    items = list_files(service=service)

    create_folder(name="alictus", items=items, service=service)

    upload(parent_dir= "alictus", metadata="./alictus/dataset.csv", uploadpath="./dataset.csv", mimetype="text/csv", items=items, service=service)
	
	# as far as I see, campaigns are distinct. 
    df = pd.read_csv('dataset.csv')
    for index, row in df.iterrows():
    	print(index, row)
    	sheet_id = create_spreadsheet(title=row["Campaign name"], items=items, datasheets_service=datasheets_service, service=service)
    	update_spreadsheet(sheet_id, df, index, items, datasheets_service, service)

if __name__ == '__main__':
    main()